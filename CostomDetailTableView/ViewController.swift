//
//  ViewController.swift
//  CostomDetailTableView
//
//  Created by PRIYA GONGAL on 30/07/19.
//  Copyright © 2019 Priya Gongal. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableViewCostom: UITableView!
    
    var array1 = ["AAA","BBB","CCC","DDD","EEE","FFF","GGG","HHH"]
    var array2 = ["aaa","bbb","ccc","ddd","eee","fff","ggg","hhh"]
   // var  arrimg = ["c1.jpg","c2.jpg","c3.jpg","c4.jpg","c5.jpg","c6.jpg","c7.jpg","c8.jpg"]
   // var arrimg = [GL_IMG_read_format]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array2.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TableViewCell
        cell.label1.text = array1[indexPath.row]
        cell.label2.text = array2[indexPath.row]
       // cell.img.image = arrimg [indexPath.row]
        return cell
    }

    
}

